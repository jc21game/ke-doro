#include "ThiefWinResult.h"
#include"Engine/SceneManager.h"
#include "Engine/Image.h"
#include "Engine/Text.h"
#include "Engine/Direct3D.h"
#include "Engine/Audio.h"

//コンストラクタ
ThiefWinResult::ThiefWinResult(IGameObject * parent)
	: IGameObject(parent, "ThiefWinResult"), pText_(nullptr), cnt_(COUNT_NUM - 1), frame_(FRAME_MIN)
{
}

//初期化
void ThiefWinResult::Initialize()
{
	std::string fileName[6] =
	{
		"Data/pict/Result_PoliceLose.jpg",
		"Data/pict/Result_ThiefWin.jpg",
		"Data/pict/Result_ThiefWin.jpg",
		"Data/pict/Result_PoliceLose_Logo.png",
		"Data/pict/Result_ThiefWin_Logo.png",
		"Data/pict/Result_ThiefWin_Logo.png"
	};

	for (int i = 0; i < 6; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}
	//hImage_ = Image::Load("Data/pict/Result_ThiefWin.jpg");

	//テキストを作成
	pText_ = new Text("ＭＳ ゴシック", 100);
	pText2_ = new Text("ＭＳ ゴシック", 100);
	pText3_ = new Text("ＭＳ ゴシック", 100);

	sec_[0] = "0";
	sec_[1] = "1";
	sec_[2] = "2";
	sec_[3] = "3";
	sec_[4] = "4";
	sec_[5] = "5";

	pText_->SetColor(0, 0, 0, 255);
	pText2_->SetColor(0, 0, 0, 255);
	pText3_->SetColor(0, 0, 0, 255);

	Audio::Play("result");

}

//更新
void ThiefWinResult::Update()
{
	//カウントダウンが0になったらタイトルシーンへ移行
	if (sec_[cnt_] == "0")
	{
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	//SPACEボタンを押すとタイトルシーンへ移行
	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	//1PコントローラーのSTRATボタンを押すとタイトルシーンへ移行
	if (Input::IsPadButtonDown(XINPUT_GAMEPAD_START))
	{
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	//60フレームごとにカウントを1減らす
	frame_++;
	if (frame_ == FRAME_MAX)
	{
		cnt_--;
		frame_ = FRAME_MIN;
	}
}

//描画
void ThiefWinResult::Draw()
{
	/*Image::SetMatrix(hImage_[i], worldMatrix_);
	Image::Draw(hImage_[3]);*/
	
	//pText_->Draw(900, 900, "タイトルシーンまであと" + sec_[cnt_] + "秒");
}

//開放
void ThiefWinResult::Release()
{
	delete pText_;
	Audio::Stop("result");
}

void ThiefWinResult::DrawSub()
{
	for (int i = 0; i < 3; i++)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, i * 1920, 0, 0);

		Image::SetMatrix(hImage_[i], localMatrix_ * m);
		Image::Draw(hImage_[i]);
	}

	for (int i = 3; i < 6; i++)
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, (i - 3) * 1920, 0, 0);

		Image::SetMatrix(hImage_[i], localMatrix_ * m);
		Image::Draw(hImage_[i]);
	}


	//左画面
	{

		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//中心画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}

	}

	//右画面
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = g.screenWidth * 2 / 3;
		vp.Y = 0;
		vp.Width = g.screenWidth / 3;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);

		//シーンのオブジェクトを表示
		for (auto it = childList_.begin(); it != childList_.end(); it++)
		{
			(*it)->DrawSub();
		}
	}

	//戻す
	{
		//ビューポート矩形
		D3DVIEWPORT9 vp;
		vp.X = 0;
		vp.Y = 0;
		vp.Width = g.screenWidth;
		vp.Height = g.screenHeight;
		vp.MinZ = 0;
		vp.MaxZ = 1;
		Direct3D::pDevice->SetViewport(&vp);
	}
}
