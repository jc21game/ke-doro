#pragma once
#include "Global.h"
#include "IGameObject.h"

class Collider
{
	//あたり判定の中心の位置
	D3DXVECTOR3 center_;
	//半径
	float		radius_;
	//誰のオブジェクトなのか
	IGameObject* owner_;

public:
	Collider(IGameObject *owner,D3DXVECTOR3 center, float radius);
	~Collider();

	//当たり判定
	//引数::相手のあたり判定
	//戻り値 :当たったかどうか
	bool IsHit(Collider	&target);

};

