#include "Sprite.h"
#include"Direct3D.h"

//コンストラクタの中に書くのと外に書くのでは外に書くほうがはやいらしい
Sprite::Sprite() : 
	pSprite_(nullptr),pTexture_(nullptr)
{
}

//デストラクタ
Sprite::~Sprite()
{
	SAFE_RELEASE(pSprite_);
	SAFE_RELEASE(pTexture_);

}

void Sprite::Load(const char* fileName)
{
	//スプライト作成
	//スプライトに大きさという概念はないらしい
	//引数 : シールはる場所があるウィンドウ、はるスプライト
	D3DXCreateSprite(Direct3D::pDevice, &pSprite_);
	assert(pSprite_ != nullptr);

	//テクスチャ作成
	//最終的にpTexture_にはういる
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);
}

void Sprite::FadeDraw(const D3DXMATRIX& matrix, int alpha)
{
	
	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND		);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DCOLOR_ARGB(alpha, alpha,alpha,alpha));
	pSprite_->End();
}

void Sprite::Draw(const D3DXMATRIX& matrix)
{

	//行列をセット
	pSprite_->SetTransform(&matrix);

	//ゲーム画面の描画
	pSprite_->Begin(D3DXSPRITE_ALPHABLEND);
	pSprite_->Draw(pTexture_, nullptr, nullptr, nullptr, D3DXCOLOR(1, 1, 1, 1));
	pSprite_->End();
}