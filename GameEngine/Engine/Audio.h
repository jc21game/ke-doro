#pragma once
#include <xact3.h>
#include <string>
#include <assert.h>


namespace Audio
{



	//������	
	void Initialize(void);

	void WaveBankLoad(std::string fileName);

	void SoundBankLoad(std::string fileName);

	void Play(std::string cueName);

	void Stop(std::string cueName);

	void Release();

}
