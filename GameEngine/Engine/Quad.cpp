#include "Quad.h"	
#include"Direct3D.h"

#define VERTEX_NUMBER ((sizeof Vertex)/(sizeof(vertexList)))
#define INDEX_NUMBER sizeof(vertexList) / 2

Quad::Quad()
	:pVertexBuffer_(nullptr), pIndexBuffer_(nullptr), material_({ 0 })
{
}


Quad::~Quad()
{
	//作った準とは逆に開放していく
	SAFE_RELEASE(pTexture_);
	SAFE_RELEASE(pIndexBuffer_);
	SAFE_RELEASE(pVertexBuffer_);

}

void Quad::Load(const char* fileName)
{
	//頂点情報登録
	Vertex vertexList[] =
	{
		D3DXVECTOR3(-1, 1,-1),	 D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0, 0),//0
		D3DXVECTOR3(1, 1, -1),	 D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0.25, 0),//1
		D3DXVECTOR3(1, -1, -1),  D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0.25, 0.5),//2
		D3DXVECTOR3(-1, -1, -1), D3DXVECTOR3(0, 0, -1),	D3DXVECTOR2(0,0.5),//3

				//6
		D3DXVECTOR3(-1, 1, 1),  D3DXVECTOR3(0, 0, 1),	D3DXVECTOR2(0.25, 0.5),//4
		D3DXVECTOR3(1, 1, 1),	D3DXVECTOR3(0, 0, 1),	D3DXVECTOR2(0.5, 0.5),
		D3DXVECTOR3(1, -1, 1),  D3DXVECTOR3(0, 0, 1),	D3DXVECTOR2(0.5, 1),
		D3DXVECTOR3(-1, -1, 1), D3DXVECTOR3(0, 0,1),	D3DXVECTOR2(0.25,1),

		//3
		D3DXVECTOR3(1,1,-1),	D3DXVECTOR3(1, 0, 0), D3DXVECTOR2(0.5,0),
		D3DXVECTOR3(1,1,1),		D3DXVECTOR3(1, 0, 0), D3DXVECTOR2(0.75,0),
		D3DXVECTOR3(1,-1,1),	D3DXVECTOR3(1, 0, 0),  D3DXVECTOR2(0.75,0.5),
		D3DXVECTOR3(1,-1,-1),	D3DXVECTOR3(1, 0, 0), D3DXVECTOR2(0.5,0.5),

		//4
		D3DXVECTOR3(-1,1,1),	D3DXVECTOR3(-1, 0, 0), D3DXVECTOR2(0.75,0),
		D3DXVECTOR3(-1,1,-1),	D3DXVECTOR3(-1, 0, 0), D3DXVECTOR2(1,0),
		D3DXVECTOR3(-1,-1,-1),  D3DXVECTOR3(-1, 0,0),  D3DXVECTOR2(1,0.5),
		D3DXVECTOR3(-1,-1,1),	D3DXVECTOR3(-1, 0, 0), D3DXVECTOR2(0.75,0.5),

		//5
		D3DXVECTOR3(-1,1,1),	D3DXVECTOR3(0, 1, 0), D3DXVECTOR2(0,0.5),
		D3DXVECTOR3(1,1,1),		D3DXVECTOR3(0, 1, 0), D3DXVECTOR2(0.25,0.5),
		D3DXVECTOR3(1,1,-1),	D3DXVECTOR3(0, 1,0),  D3DXVECTOR2(0.25,1),
		D3DXVECTOR3(-1,1,-1),	D3DXVECTOR3(0, 1, 0), D3DXVECTOR2(0,1),

		//2
		D3DXVECTOR3(-1,-1,-1),  D3DXVECTOR3(0, -1, 0), D3DXVECTOR2(0.25,0),
		D3DXVECTOR3(1,-1,-1),	D3DXVECTOR3(0, -1, 0), D3DXVECTOR2(0.5,0),
		D3DXVECTOR3(1,-1,1),	D3DXVECTOR3(0, -1,0),  D3DXVECTOR2(0.5,0.5),
		D3DXVECTOR3(-1,-1,1),	D3DXVECTOR3(0, -1, 0), D3DXVECTOR2(0.25,0.5),
	};



	//バッファに情報を登録
	//第一引数は頂点情報分の領域
	//D3DFVF_XYZ 位置情報のこと
	//D3DFVF_DIFFUSE 拡散反射光　色のこと
	Direct3D::pDevice->CreateVertexBuffer(sizeof(vertexList), 0,
		D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1, D3DPOOL_MANAGED, &pVertexBuffer_, 0);
	//Create関数を使ったのでassertを使う
	assert(pVertexBuffer_ != nullptr);

	Vertex *vCopy;
	//Lockは仕様なのでなやまなくてよい
	pVertexBuffer_->Lock(0, 0, (void**)&vCopy, 0);
	//１：コピーしたものを入れる場所	2:コピーされる場所3:コピーする大きさ
	memcpy(vCopy, vertexList, sizeof(vertexList));
	//情報をすでに書き込
	pVertexBuffer_->Unlock();

	//作る三角形の頂点を順番に入力する
	//入力する際は頂点の時計回りに入力
	//int indexList[] = { 0, 2, 3, 0, 1, 2 ,5,4,7,5,7,6,8,9,10,8,10,11,12,13,14,12,14,15,16,17,16,19,17,19,18};
	int indexList[] = { 0, 2, 3, 0, 1, 2,5, 4, 7, 5, 7, 6, 8, 9, 10, 8, 10, 11, 12, 13, 14, 12, 14, 15,16,17,18,16,18,19,20,21,22,20,22,23 };

	//上に同じ
	Direct3D::pDevice->CreateIndexBuffer(sizeof(indexList), 0, D3DFMT_INDEX32,
		D3DPOOL_MANAGED, &pIndexBuffer_, 0);
	assert(pIndexBuffer_ != nullptr);

	DWORD *iCopy;
	pIndexBuffer_->Lock(0, 0, (void**)&iCopy, 0);
	memcpy(iCopy, indexList, sizeof(indexList));
	pIndexBuffer_->Unlock();

	//マテリアルの設定
	material_.Diffuse.r = 1.0f;
	material_.Diffuse.g = 1.0f;
	material_.Diffuse.b = 1.0f;

	//テクスチャ作成
//最終的にpTexture_にはういる
	D3DXCreateTextureFromFileEx(Direct3D::pDevice, fileName,
		0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE,
		D3DX_DEFAULT, 0, 0, 0, &pTexture_);
	assert(pTexture_ != nullptr);

}

void Quad::Draw(const D3DXMATRIX& matrix)
{
	//変形を確定
	Direct3D::pDevice->SetTransform(D3DTS_WORLD, &matrix);

	//引数は表示したい場所が入ってる
	Direct3D::pDevice->SetStreamSource(0, pVertexBuffer_, 0, sizeof(Vertex));
	Direct3D::pDevice->SetTexture(0, pTexture_);
	Direct3D::pDevice->SetMaterial(&material_);
	Direct3D::pDevice->SetIndices(pIndexBuffer_);

	//第4引数は頂点データの数、最後の引数はポリゴンの数。
	Direct3D::pDevice->SetFVF(D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1);
	Direct3D::pDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, VERTEX_NUMBER, 0, INDEX_NUMBER);
}
