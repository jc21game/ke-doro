#pragma once
#include "Global.h"

class Sprite
{
	LPD3DXSPRITE				pSprite_;  //スプライト

	LPDIRECT3DTEXTURE9 pTexture_; //テクスチャ


public:
	Sprite();
	~Sprite();

	//テクスチャ＆スプライト作成準備
	//引数 : ファイル名
	//戻値 : なし
	void Load(const char* p);

	//スプライトの描画
	//引数 : 参照渡しにより4byteの受け渡しですむ
	//戻値 : なし
	void FadeDraw(const D3DXMATRIX& Matrix,int alpha);

	//スプライトの描画
	//引数 : 参照渡しにより4byteの受け渡しですむ
	//戻値 : なし
	void Draw(const D3DXMATRIX& Matrix);
};

