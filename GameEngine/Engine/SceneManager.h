#pragma once
#include "IGameObject.h"

//シーンの番号
enum SCENE_ID
{
	SCENE_ID_SPLASH,
	SCENE_ID_TITLE,
	SCENE_ID_PLAY,
	SCENE_ID_POLICEWIN,
	SCENE_ID_THIEFWIN,
};

//◆◆◆を管理するクラス
class SceneManager : public IGameObject
{
	static SCENE_ID currentSceneID_;
	static SCENE_ID nextSceneID_;

	static IGameObject* pCurrentScene_;

public:
	
	//コンストラクタ
	SceneManager(IGameObject* parent);

	//デストラクタ
	~SceneManager();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//次のシーンの情報をもらう関数
	static void ChangeScene(SCENE_ID next);

	static IGameObject* GetCurrentScene()
	{
		return  pCurrentScene_;
	}
};