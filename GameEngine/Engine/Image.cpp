#include "Image.h"
#include "Global.h"
#include<vector>

namespace Image
{
	std::vector<ImageData*> dataList;


	int Load(std::string filename)
	{
		ImageData* pData = new ImageData;
		pData->fileName = filename;

		bool isExist = false;

		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//今まで読み込んだモデルと今回読み込んだモデルの名前が同じかチェック
			if (dataList[i]->fileName == filename)
			{
				//アドレスをコピー
				pData->pSprite = dataList[i]->pSprite;
				isExist = true;
			}
		}

		if (isExist == false)
		{
			//新しいモデルならそのモデルを読み込み
			pData->pSprite = new Sprite;
			pData->pSprite->Load(filename.c_str());
		}



		//モデルデータを格納
		dataList.push_back(pData);

		//帰ってくる要素の値が１よりも０のほうが管理しやすいのでー１
		return dataList.size() - 1;
	}

	void FadeDraw(int handle,int alpha)
	{
		//データリストに格納されたfbxデータのドローを呼び出す
		dataList[handle]->pSprite->FadeDraw(dataList[handle]->matrix,alpha);
	}

	void Draw(int handle)
	{
		//データリストに格納されたfbxデータのドローを呼び出す
		dataList[handle]->pSprite->Draw(dataList[handle]->matrix);
	}

	void SetMatrix(int handle, D3DXMATRIX & matrix)
	{
		dataList[handle]->matrix = matrix;
	}

	void Release(int handle)
	{
		bool isExist = false;

		//ハンドル番目のおんなじFBXがないかチェっく同じのがあったら消さない
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//handleはLoad関数の戻り値なのでDataListの場所と紐付けられてるので自分という証明ができる
			//自分は消さないのでスキップ
			if (i == handle)
			{
				continue;
			}

			//同じFBXだったら消さない
			//そこにデータがあって　かつ　同じFBXだったら
			if (dataList[i] != nullptr && dataList[i]->pSprite == dataList[handle]->pSprite)
			{
				isExist = true;
				break;
			}
		}

		//同じじゃなかったら消す
		if (isExist == false)
		{
			SAFE_DELETE(dataList[handle]->pSprite);
		}
		SAFE_DELETE(dataList[handle]);

	}

	void AllRelase()
	{
		for (unsigned int i = 0; i < dataList.size(); i++)
		{
			//消してなかったら
			if (dataList[i] != nullptr)
			{
				Release(i);
			}

			SAFE_DELETE(dataList[i]);
		}
		dataList.clear();
	}
}

