#include "Direct3D.h"
#include "../SplashScene.h"

//bool Direct3D::movieFlag_;
//初期化
LPDIRECT3D9  Direct3D::pD3d = nullptr;
LPDIRECT3DDEVICE9	Direct3D::pDevice = nullptr;
void Direct3D::Initialize(HWND hWnd)
{
	//Direct3Dオブジェクトの作成
	//pD3d　はポインタ　でインターフェースnewをしないといけないが下はしていない
	//それはなぜか 下の○○Createはがいろいろやってくれるためかかなくでも大丈夫らしい
	//LPにはお決まりで○○Createがついているらしい
	pD3d = Direct3DCreate9(D3D_SDK_VERSION);
	assert(pD3d != nullptr);

	//DIRECT3Dデバイスオブジェクトの作成
	D3DPRESENT_PARAMETERS d3dpp;	                //専用の構造体
	ZeroMemory(&d3dpp, sizeof(d3dpp));	          //中身を全部0にする
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE; //
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.BackBufferWidth = g.screenWidth;
	d3dpp.BackBufferHeight = g.screenHeight;
	pD3d->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,	//
		D3DCREATE_HARDWARE_VERTEXPROCESSING, &d3dpp, &pDevice); //&pDeviceがゲーム画面になったと思えばいいらしい　これだとまだウィンドウに貼り付けてにないのでウィンドウにはでない
	assert(pDevice != nullptr);


	//アルファブレンド(今きにしなくいい)半透明に表示するかどうか
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);

	

	//ライティング
	pDevice->SetRenderState(D3DRS_LIGHTING, TRUE);

	//ライトを設置
	D3DLIGHT9 lightState[6];
	ZeroMemory(&lightState, sizeof(lightState));

	//ライトが向いてる方向
	lightState[0].Direction = D3DXVECTOR3(0, 0, 1);
	lightState[1].Direction = D3DXVECTOR3(0, 1, 0);
	lightState[2].Direction = D3DXVECTOR3(1, 0, 0);
	lightState[3].Direction = D3DXVECTOR3(0, 0, -1);
	lightState[4].Direction = D3DXVECTOR3(0, -1, 0);
	lightState[5].Direction = D3DXVECTOR3(-1, 0, 0);

	//平行光源
	for (int i = 0; i < 6; i++)
	{
		lightState[i].Type = D3DLIGHT_DIRECTIONAL;

		//ライトの色
		if (i < 3)
		{
			lightState[i].Diffuse.r = 0.9f;
			lightState[i].Diffuse.g = 0.9f;
			lightState[i].Diffuse.b = 0.9f;
		}
		else {
			lightState[i].Diffuse.r = 1.0f;
			lightState[i].Diffuse.g = 1.0f;
			lightState[i].Diffuse.b = 1.0f;
		}


		//この設定でライトをさくせい
		pDevice->SetLight(i, &lightState[i]);
		//ライトをON
		pDevice->LightEnable(i, TRUE);
	}

	//カメラ
	D3DXMATRIX view;
	//ビュー行列を作るための関数(１、入れる箱　２、カメラの位置（視点）３、どこをみるか（焦点） 4上方向ベクトル（上を示す）)
	D3DXMatrixLookAtLH(&view, &D3DXVECTOR3(0, 5, -10), &D3DXVECTOR3(0, 0, 0), &D3DXVECTOR3(0, 1, 0));
	//(1,ビュー行列として使ってほしい情報)
	pDevice->SetTransform(D3DTS_VIEW, &view);
	D3DXMATRIX proj;
	//プロジェクション行列をつくるための関数（１、入れる箱　２　視野角(ズーム)　３、アスペクト比 4,どこから先を表示するか(ニアクリッピング面),５何メートル先まで移すか（ファークリッピング面）
	//４、５の差はなるべくないほうがいい
	D3DXMatrixPerspectiveFovLH(&proj, D3DXToRadian(60),1 , 0.5f, 1000.0f);
	//
	pDevice->SetTransform(D3DTS_PROJECTION, &proj);
	

}

void Direct3D::BeginDraw()
{
	//画面をクリア(毎フレーム)
	pDevice->Clear(0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB(157, 204, 224), 1.0f, 0);

	//描画開始
	pDevice->BeginScene();
}

void Direct3D::EndDraw()
{
	//描画終了
	pDevice->EndScene();

	//動画が再生されていた場合はこの処理を無視する
	if ( !g.movieFlag_ )
	{
		//スワップ
		//バックバッファとフロントバッファなるものが存在しこの二つを交換して表示する事でちらつかない表示ができるらしい
		pDevice->Present(NULL, NULL, NULL, NULL);
	}
}

void Direct3D::Release()
{
	//開放処理
	//LP○○にはReleaseという開放関数がセットであるのでそれを使う
	//開放処理は作った順と逆に開放する
	pDevice->Release();
	pD3d->Release();
}
