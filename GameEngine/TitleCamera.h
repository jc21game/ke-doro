#pragma once
#include "Engine/IGameObject.h"

//◆◆◆を管理するクラス
class TitleCamera : public IGameObject
{
	D3DXMATRIX view_; //メンバビューk
public:
	//コンストラクタ
	TitleCamera(IGameObject* parent);

	//デストラクタ
	~TitleCamera();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//ビュー受け取り用
	D3DXMATRIX GetView() { return view_; }
};