#pragma once

#include "Engine/global.h"
#include "TitleCamera.h"

class TitleCamera;
class Thief;
class Police;

//タイトルシーンを管理するクラス
class TitleScene : public IGameObject
{
	//フェードイン用
	int alpha_;
	
	//イメージ管理番号
	int hPict_;
	int hPict2_;

	//各プレイヤーのポインタ
	Police* pPolice_;
	Thief*  pThief1_;

	//タイトルのカメラ
	TitleCamera* pTitleCamera_;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TitleScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//描画(画面分割用)
	void DrawSub()	override;
};