#include "SplashScene.h"
#include <time.h>
#include "Engine/Movie.h"
#include "Engine/Direct3D.h"
#include "Engine/SceneManager.h"

//コンストラクタ
SplashScene::SplashScene(IGameObject * parent)
	: IGameObject(parent, "SplashScene"), countFrame_(0)
{
}

//初期化
void SplashScene::Initialize()
{
	//動画の読み込み
	Movie::Load(L"Data/Movie/Splash_TeamLogo1.avi");
	//動画の再生
	Movie::Play();
	//動画が再生された時に再生中のフラグを立てる
	g.movieFlag_ = true;

	hPict_ = Image::Load("Data/pict/Loading.jpg");
	hPict2_ = Image::Load("Data/pict/Loading_2.jpg");
}

//更新
void SplashScene::Update()
{

	if (countFrame_ > Movie::GetMovieLength() || Input::IsKey(DIK_SPACE))
	{
		//動画を止める
		Movie::Stop();
		//再生中のフラグを元に戻す
		g.movieFlag_ = false;
		//動画の再生が終わったらシーン切り替え
		SceneManager::ChangeScene(SCENE_ID_TITLE);	

	}

	if (countFrame_ > Movie::GetMovieLength() || Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 0))
	{
		//動画を止める
		Movie::Stop();
		//再生中のフラグを元に戻す
		g.movieFlag_ = false;
		//動画の再生が終わったらシーン切り替え
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	else if (countFrame_ > Movie::GetMovieLength() || Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 1))
	{
		//動画を止める
		Movie::Stop();
		//再生中のフラグを元に戻す
		g.movieFlag_ = false;
		//動画の再生が終わったらシーン切り替え
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	else if (countFrame_ > Movie::GetMovieLength() || Input::IsPadButtonDown(XINPUT_GAMEPAD_START, 2))
	{
		//動画を止める
		Movie::Stop();
		//再生中のフラグを元に戻す
		g.movieFlag_ = false;
		//動画の再生が終わったらシーン切り替え
		SceneManager::ChangeScene(SCENE_ID_TITLE);
	}

	countFrame_++;

}

//描画
void SplashScene::Draw()
{
	if (!g.movieFlag_)
	{
	
		D3DXMATRIX m1;
		D3DXMatrixTranslation(&m1, 0 * 1920, 0, 0);

		Image::SetMatrix(hPict2_, localMatrix_ * m1);
		Image::Draw(hPict2_);


		for (int i = 1; i < 3; i++)
		{
			D3DXMATRIX m;
			D3DXMatrixTranslation(&m, i * 1920, 0, 0);

			Image::SetMatrix(hPict_, localMatrix_ * m);
			Image::Draw(hPict_);

		}
		



		////左画面
		//{

		//	//ビューポート矩形
		//	D3DVIEWPORT9 vp;
		//	vp.X = 0;
		//	vp.Y = 0;
		//	vp.Width = g.screenWidth / 3;
		//	vp.Height = g.screenHeight;
		//	vp.MinZ = 0;
		//	vp.MaxZ = 1;
		//	Direct3D::pDevice->SetViewport(&vp);

		//	//ビュー行列
		//	//Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//	//シーンのオブジェクトを表示
		//	for (auto it = childList_.begin(); it != childList_.end(); it++)
		//	{
		//		(*it)->DrawSub();
		//	}

		//}

		////中心画面
		//{
		//	//ビューポート矩形
		//	D3DVIEWPORT9 vp;
		//	vp.X = g.screenWidth / 3;
		//	vp.Y = 0;
		//	vp.Width = g.screenWidth / 3;
		//	vp.Height = g.screenHeight;
		//	vp.MinZ = 0;
		//	vp.MaxZ = 1;
		//	Direct3D::pDevice->SetViewport(&vp);

		//	//ビュー行列
		//	//Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//	//シーンのオブジェクトを表示
		//	for (auto it = childList_.begin(); it != childList_.end(); it++)
		//	{
		//		(*it)->DrawSub();
		//	}

		//}

		////右画面
		//{
		//	//ビューポート矩形
		//	D3DVIEWPORT9 vp;
		//	vp.X = g.screenWidth * 2 / 3;
		//	vp.Y = 0;
		//	vp.Width = g.screenWidth / 3;
		//	vp.Height = g.screenHeight;
		//	vp.MinZ = 0;
		//	vp.MaxZ = 1;
		//	Direct3D::pDevice->SetViewport(&vp);

		//	//ビュー行列
		//	//Direct3D::pDevice->SetTransform(D3DTS_VIEW, &pTitleCamera_->GetView());

		//	//シーンのオブジェクトを表示
		//	for (auto it = childList_.begin(); it != childList_.end(); it++)
		//	{
		//		(*it)->DrawSub();
		//	}
		//}

		////戻す
		//{
		//	//ビューポート矩形
		//	D3DVIEWPORT9 vp;
		//	vp.X = 0;
		//	vp.Y = 0;
		//	vp.Width = g.screenWidth;
		//	vp.Height = g.screenHeight;
		//	vp.MinZ = 0;
		//	vp.MaxZ = 1;
		//	Direct3D::pDevice->SetViewport(&vp);
		//}
	}

}

//開放
void SplashScene::Release()
{
	
}